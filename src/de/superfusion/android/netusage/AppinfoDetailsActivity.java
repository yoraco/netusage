package de.superfusion.android.netusage;

import android.content.pm.*;
import android.os.*;
import android.support.v4.app.*;
import android.text.*;
import android.net.*;

public class AppinfoDetailsActivity extends FragmentActivity
	{
		public static final String EXTRA_APPINFO_PACKAGE_STRING="de.superfusion.android.netusage.EXTRA_APPINFO_PACKAGE_STRING";
		public static final String EXTRA_APPINFO_TOTAL_DWN_LONG="de.superfusion.android.netusage.EXTRA_APPINFO_TOTAL_DWN_LONG";
		public static final String EXTRA_APPINFO_TOTAL_UP_LONG="de.superfusion.android.netusage.EXTRA_APPINFO_TOTAL_UP_LONG";

		@Override
		protected void onCreate(Bundle savedInstanceState)
			{
				super.onCreate(savedInstanceState);
				setContentView(R.layout.appinfo_details_activity);
				Bundle extras=getIntent().getExtras();
				if (null != extras)
					{
						AppinfoDetailsFragment detailsFragment=(AppinfoDetailsFragment)getSupportFragmentManager().findFragmentById(R.id.appinfo_details_fragment);
						AppinfoListFragment.AppInfo appInfo=null;
						String packageName=extras.getString(EXTRA_APPINFO_PACKAGE_STRING, "");
						if (!TextUtils.isEmpty(packageName))
							{
								try
									{
										ApplicationInfo applicationInfo= getApplicationContext().getPackageManager().getApplicationInfo(packageName, PackageManager.GET_META_DATA);
										appInfo = AppinfoListFragment.getAppInfoThatHasTraffic(this, applicationInfo);
										
										long totalDownload=extras.getLong(EXTRA_APPINFO_TOTAL_DWN_LONG, TrafficStats.getTotalRxBytes());
										long totalUpload=extras.getLong(EXTRA_APPINFO_TOTAL_UP_LONG, TrafficStats.getTotalTxBytes());
										appInfo.totalDownload = totalDownload;
										appInfo.totalUpload = totalUpload;
									}
								catch (Exception e)
									{
										e.printStackTrace();
									}
							}
						if (null != appInfo)
							{
								detailsFragment.update(appInfo);
							}
						else
							{
								finish();
							}
					}
				else
					{
						finish();
					}
			}
	}
