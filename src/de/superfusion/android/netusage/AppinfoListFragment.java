package de.superfusion.android.netusage;
import android.app.*;
import android.content.pm.*;
import android.graphics.drawable.*;
import android.net.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import java.io.*;
import java.text.*;
import java.util.*;
import android.view.View.*;


public class AppinfoListFragment extends android.support.v4.app.Fragment
implements AdapterView.OnItemClickListener	

	{

		AppinfoCommunicator appinfoCommunicator;

		public interface AppinfoCommunicator
			{
				public void update(AppInfo appInfo);
			}

		@Override
		public void onAttach(Activity activity)
			{
				super.onAttach(activity);
				if (activity instanceof AppinfoCommunicator)
					{
						appinfoCommunicator = (AppinfoCommunicator) activity;
					}
				else
					{
						throw new ClassCastException(String.valueOf(activity.getClass().getName()) + " must implement AppinfoListFragment.AppinfoCommunicator");
					}
			}

		@Override
		public void onItemClick(AdapterView<?> p1, View p2, int p3, long p4)
			{
				AppInfo appInfo=listAppInfo.get(p3);
				appinfoCommunicator.update(appInfo);
			}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
			{
				View view=inflater.inflate(R.layout.appinfo_list_fragment, container, false);
				listView = (ListView)view.findViewById(R.id.appinfo_listview);
				listView.setOnItemClickListener(this);
				loadEntries();
				return view;
			}

		ListView listView;
		List<AppInfo> listAppInfo=new ArrayList<AppInfo>();
		AppListAdapter adapter;
		AsyncTask<Void,ApplicationInfo,List<AppInfo>> loadingTask;
		enum ListOrder
			{
				orderByName,//
				orderByTotal,//
				orderByUpload,//
				orderByDownload//
				}
		ListOrder order=ListOrder.orderByTotal;
		/**
		 * {@code -1} for DESC.<br/>
		 * {@code 1} for ASC.<br/>
		 */
		private int orderFac=-1;


		void loadEntries()
			{
				if (null != loadingTask)
					{
						loadingTask.cancel(true);
						loadingTask = null;
					}
				final android.content.Context _context=getActivity();
				loadingTask = new AsyncTask<Void,ApplicationInfo,List<AppInfo>>(){
						ProgressDialog pgd;
						List<ApplicationInfo> _listAppInfo;
						@Override
						protected void onPreExecute()
							{
								pgd = new ProgressDialog(_context);
								pgd.setCancelable(false);
								pgd.setIndeterminate(true);
								pgd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
								pgd.show();
							}

						@Override
						protected void onProgressUpdate(ApplicationInfo... appInfos)
							{
								if (null != _listAppInfo && null != appInfos)
									{
										pgd.setProgress(appInfos.length);
										pgd.setMax(_listAppInfo.size());
										pgd.setIndeterminate(false);
										pgd.setMessage("Processing apps...");
										pgd.setTitle("Processing apps...");
									}
								else
									{
										pgd.setMessage("Creating app list...");
										pgd.setTitle("Creating app list...");
										pgd.setIndeterminate(true);
									}
							}

						@Override
						protected List<AppInfo> doInBackground(Void... params)
							{
								publishProgress(null);
								List<AppInfo> listAppInfo = new ArrayList<AppInfo>();

								if (isCancelled())
									return new ArrayList<AppInfo>();

								_listAppInfo = getActivity().getPackageManager().getInstalledApplications(PackageManager.GET_META_DATA);

								if (isCancelled())
									return new ArrayList<AppInfo>();

								//keep only unique uids
								List<AppInfo> remove=new ArrayList<AppInfo>();
								List<Integer> knownIds=new ArrayList<Integer>();

								List<ApplicationInfo> processing=new ArrayList<ApplicationInfo>();
								for (ApplicationInfo aI:_listAppInfo)
									{
										processing.add(aI);
										publishProgress(processing.toArray(new ApplicationInfo[processing.size()]));

										if (isCancelled())
											return new ArrayList<AppInfo>();

										AppInfo appInfo=getAppInfoThatHasTraffic(getActivity(), aI);
										if (null != appInfo)
											{
												listAppInfo.add(appInfo);
											}

										if (isCancelled())
											return new ArrayList<AppInfo>();	

										if (null != appInfo)
											if (!knownIds.contains(appInfo.appInfo.uid))
												{
													knownIds.add(appInfo.appInfo.uid);
												}
											else
												{
													remove.add(appInfo);
												}

									}

								listAppInfo.removeAll(remove);
								return listAppInfo;
							}

						@Override
						protected void onCancel(List<AppInfo> listAppInfo)
							{
								pgd.dismiss();
								setListAppInfo(listAppInfo);
							}

						@Override
						protected void onPostExecute(List<AppInfo> listAppInfo)
							{
								pgd.dismiss();
								setListAppInfo(listAppInfo);
							}
					};
				loadingTask.execute();
			}

		public static AppInfo getAppInfoThatHasTraffic(android.content.Context context, ApplicationInfo aI)
			{
				AppInfo appInfo=new AppInfo();
				long downloaded=TrafficStats.getUidRxBytes(aI.uid);
				long uploaded=TrafficStats.getUidTxBytes(aI.uid);

				// filter system applications only 
				if ((aI.flags & ApplicationInfo.FLAG_SYSTEM) != 0)
					{
						appInfo.systemApp = true;
						// check if application has network usage 
						if (downloaded > 0 || uploaded > 0)
							{
								// it's application you want 
							} 
					} 
				// non-system application 
				else 
					{ 
						appInfo.systemApp = false;
						if (downloaded > 0 || uploaded > 0)
							{
								// it's application you want 
							} 
					}
				if (downloaded > 0 || uploaded > 0)
					{
						appInfo.appInfo = aI;
						appInfo.downloaded = downloaded;
						appInfo.uploaded = uploaded;
						appInfo.label = String.valueOf(context.getPackageManager().getApplicationLabel(appInfo.appInfo));
						appInfo.icon = context.getPackageManager().getApplicationIcon(appInfo.appInfo);
						appInfo.icon.setBounds(0, 0, appInfo.icon.getIntrinsicWidth(), appInfo.icon.getIntrinsicHeight());
						long totalTrafficDownload=TrafficStats.getTotalRxBytes();
						long totalTrafficUpload=TrafficStats.getTotalTxBytes();
						appInfo.totalDownload = totalTrafficDownload;
						appInfo.totalUpload = totalTrafficUpload;
						return appInfo;
					}
				return null;
			}	

		void setListAppInfo(List<AppInfo> listAppInfo)
			{
				this.listAppInfo = listAppInfo;

				this.adapter = new AppListAdapter(getActivity(), 
												  R.layout.appinfo_list_entry, listAppInfo);				
				this.listView.setAdapter(adapter);

				switch (this.order)
					{
						case orderByName:
							orderByName();
							break;
						case orderByTotal:
							orderByTotal();
							break;
						case orderByUpload:
							orderByUpload();
							break;
						case orderByDownload:
							orderByDownload();
							break;
					}
			}

		void orderByUpload()
			{
				order = ListOrder.orderByUpload;
				Collections.sort(listAppInfo, new Comparator<AppInfo>(){
							public int compare(AppInfo l, AppInfo r)
								{
									return ((int)((l.uploaded) - (r.uploaded))) * orderFac;
								}
						});
				adapter.notifyDataSetChanged();
			}
		void orderByDownload()
			{
				order = ListOrder.orderByDownload;
				Collections.sort(listAppInfo, new Comparator<AppInfo>(){
							public int compare(AppInfo l, AppInfo r)
								{
									return ((int)((l.downloaded) - (r.downloaded))) * orderFac;
								}
						});
				adapter.notifyDataSetChanged();
			}
		void orderByTotal()
			{
				order = ListOrder.orderByTotal;
				Collections.sort(listAppInfo, new Comparator<AppInfo>(){
							public int compare(AppInfo l, AppInfo r)
								{
									return ((int)((l.getTotal()) - (r.getTotal()))) * orderFac;
								}
						});
				adapter.notifyDataSetChanged();
			}
		void orderByName()
			{
				order = ListOrder.orderByName;
				Collections.sort(listAppInfo, new Comparator<AppInfo>(){
							public int compare(AppInfo l, AppInfo r)
								{
									return String.valueOf(l.label).compareTo(String.valueOf(r.label)) * (orderFac * -1);
								}
						});
				adapter.notifyDataSetChanged();
			}

		public static class AppInfo implements Serializable
			{
				ApplicationInfo appInfo;
				String label;
				long downloaded;
				long uploaded;
				long totalUpload;
				long totalDownload;
				boolean systemApp;
				Drawable icon;
				@Override
				public String toString()
					{
						StringBuilder sb=new StringBuilder();
						sb.append(label);
						sb.append("\n");
						sb.append(appInfo.packageName);
						sb.append(" ");
						sb.append("\n");
						sb.append("u:" + getUploadString());
						sb.append(", ");
						sb.append("d:" + getDownloadString());
						return sb.toString();
					}
				private boolean si=false;	
				public float getPercentageFromTotal()
					{
						float G = totalDownload + totalUpload;
						float P = downloaded + uploaded;
						float p = (100 / G) * P;
						return p;
					}
				public long getTotal()
					{
						return uploaded + downloaded;
					}
				public String getTotalString()
					{
						NumberFormat nf = new DecimalFormat("#.##");
						return getHumanReadableByteString(getTotal(), si)
							+ " " + nf.format(getPercentageFromTotal()) 
							+ "%";
					}
				public String getUploadString()
					{
						return getHumanReadableByteString(uploaded, si);
					}
				public String getDownloadString()
					{
						return getHumanReadableByteString(downloaded, si);
					}
				public String getHumanReadableByteString(long bytes, boolean si)
					{
						int unit = si ? 1000 : 1024; 
						if (bytes < unit) return bytes + " B"; 
						int exp = (int) (Math.log(bytes) / Math.log(unit)); 
						String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "i"); 
						return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
					}
			}

		class AppListAdapter extends ArrayAdapter<AppInfo>
			{
				class ViewHolder
					{
						TextView title;
						TextView subtitle;
						ImageView icon;
						TextView upload;
						TextView download;
						TextView total;
						View averageOutline;
						View averageInline;
						View averageInlinePlaceholder;
					}

				private LayoutInflater inflater=null;
				public AppListAdapter(android.content.Context context, int resource, java.util.List<AppInfo> objects)
					{
						super(context, resource, objects);
						inflater = ( LayoutInflater )context.getSystemService(android.content.Context.LAYOUT_INFLATER_SERVICE);
					}

				@Override
				public View getView(int position, View convertView, ViewGroup parent)
					{
						View view=convertView;
						ViewHolder holder=null;
						if (null == convertView)
							{
								view = inflater.inflate(R.layout.appinfo_list_entry, null);
								holder = new ViewHolder();
								holder.icon = (ImageView) view.findViewById(R.id.appinfo_icon);
								holder.title = (TextView)view.findViewById(R.id.appinfo_title);
								holder.subtitle = (TextView)view.findViewById(R.id.appinfo_subtitle);
								holder.upload = (TextView)view.findViewById(R.id.appinfo_upload);
								holder.download = (TextView)view.findViewById(R.id.appinfo_download);
								holder.total = (TextView)view.findViewById(R.id.appinfo_total);
								holder.averageOutline = view.findViewById(R.id.appinfo_list_entry_average_percent_outline);
								holder.averageInline = view.findViewById(R.id.appinfo_list_entry_average_percent_inline);
								holder.averageInlinePlaceholder = view.findViewById(R.id.appinfo_list_entry_average_percent_inline_ph);
								view.setTag(holder);
							}
						else
							{
								holder = (ViewHolder)view.getTag();
							}

						AppInfo aInfo=getItem(position);
						holder.title.setText(aInfo.label);
						holder.subtitle.setText(aInfo.appInfo.uid + " - " + aInfo.appInfo.packageName);
						holder.icon.setImageDrawable(aInfo.icon);
						holder.upload.setText(aInfo.getUploadString());
						holder.download.setText(aInfo.getDownloadString());
						holder.total.setText(aInfo.getTotalString());

						// use layout weight to set percentag 
						// by using parent views attribute 'weightSum' set to 100
						float left=aInfo.getPercentageFromTotal();
						if (left < 0.5)
							left = 0.5F;
						float right=100 - left;

						float layout_weight_left=right;
						float layout_weight_right=left;
						holder.averageInline.setLayoutParams(
							new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
														  holder.averageInline.getLayoutParams().height,
														  layout_weight_left
														  ));

						holder.averageInlinePlaceholder.setLayoutParams(
							new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
														  holder.averageInline.getLayoutParams().height,
														  layout_weight_right
														  ));
						return view;
					}

			}

	}
