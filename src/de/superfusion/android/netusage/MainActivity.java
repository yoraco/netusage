package de.superfusion.android.netusage;

import android.content.*;
import android.os.*;
import android.support.v4.app.*;
import android.widget.*;

public class MainActivity extends FragmentActivity implements AppinfoListFragment.AppinfoCommunicator
	{
		private static final String TAG="NETUSAGE";

		/** Called when the activity is first created. */
		@Override
		public void onCreate(Bundle savedInstanceState)
			{
				super.onCreate(savedInstanceState);
				setContentView(R.layout.main);

			}

		public void update(AppinfoListFragment.AppInfo appInfo)
			{
				//Toast.makeText(this, String.valueOf(appInfo), Toast.LENGTH_LONG).show();
				AppinfoDetailsFragment detailsFragment=(AppinfoDetailsFragment)getSupportFragmentManager().findFragmentById(R.id.appinfo_details_fragment);
				if (null != detailsFragment && detailsFragment.isInLayout())
					{
						detailsFragment.update(appInfo);
					}
				else
					{
						// start details view as activity here...
						Intent intent=new Intent(getApplicationContext(), AppinfoDetailsActivity.class);
						intent.putExtra(AppinfoDetailsActivity.EXTRA_APPINFO_PACKAGE_STRING, appInfo.appInfo.packageName);
						startActivity(intent);
					}
			}
	}
