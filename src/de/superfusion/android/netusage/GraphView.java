package de.superfusion.android.netusage;

import android.content.*;
import android.graphics.*;
import android.util.*;
import android.view.*;
import java.util.*;

public class GraphView extends View
	{

		private Paint paint=new Paint(Paint.ANTI_ALIAS_FLAG);
		private float[] values_degrees=new float[]{180,180};
		private int[] colors=null;
		private int desiredWidth=256;
		private int desiredHeight=256;
		private RectF positionOfGraph=new RectF(120, 120, 380, 380);

		public GraphView(Context context, AttributeSet attributeSet)
			{
				super(context, attributeSet);
			}

		@Override
		protected void onDraw(Canvas canvas)
			{
				super.onDraw(canvas);
				float currPosition=0;
				for (int i=0;i < values_degrees.length;i++)
					{
						if (null == colors)
							{
								Random random = new Random();
								int color=Color.argb(
									(i == 0 ?100: 255)
									, random.nextInt(256), random.nextInt(256), random.nextInt(256));
								paint.setColor(color);
							}
						else
							{
								paint.setColor(colors[i]);
							}
						canvas.drawArc(positionOfGraph, currPosition, values_degrees[i], true, paint);
						currPosition += values_degrees[i];
					}
			}
		/**
		 * set the appropriate size of the drawing rectangle
		 */
		@Override
		protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
			{

				int desiredWidth = 100;
				int desiredHeight = 100;
				desiredWidth = this.desiredWidth;
				desiredHeight = this.desiredHeight;


				int widthMode = MeasureSpec.getMode(widthMeasureSpec);
				int widthSize = MeasureSpec.getSize(widthMeasureSpec);
				int heightMode = MeasureSpec.getMode(heightMeasureSpec);
				int heightSize = MeasureSpec.getSize(heightMeasureSpec);

				int width;
				int height;

				//Measure Width
				if (widthMode == MeasureSpec.EXACTLY)
					{
						//Must be this size
						width = widthSize;
					}
				else if (widthMode == MeasureSpec.AT_MOST)
					{
						//Can't be bigger than...
						width = Math.min(desiredWidth, widthSize);
					}
				else
					{
						//Be whatever you want
						width = desiredWidth;
					}

				//Measure Height
				if (heightMode == MeasureSpec.EXACTLY)
					{
						//Must be this size
						height = heightSize;
					}
				else if (heightMode == MeasureSpec.AT_MOST)
					{
						//Can't be bigger than...
						height = Math.min(desiredHeight, heightSize);
					}
				else
					{
						//Be whatever you want
						height = desiredHeight;
					}

				//MUST CALL THIS
				setMeasuredDimension(width, height);

				positionOfGraph = new RectF(0, 0, width, height);
			}
		/**
		 * @param values
		 * @param colors May be {@code null}.<br/>
		 * If not {@code null} then it must be same size as param 'values'.<br/>
		 * If {@code null} then random colors will be used.<br/>
		 *
		 */
		public void updateFromValues(final float[] values, final int[] colors)
			{
				if (colors != null && colors.length != values.length)
					throw new IllegalArgumentException("Parameter 'colors' must either be null or have the same size as parameter 'values'!");
				this.colors = colors;
				this.values_degrees = calculateDegrees(values);
				this.invalidate();
			}
		/**
		 * @param values_degrees
		 * @param colors May be {@code null}.<br/>
		 * If not {@code null} then it must be same size as param 'values_degrees'.<br/>
		 * If {@code null} then random colors will be used.<br/>
		 *
		 */
		public void updateFromDegrees(final float[] values_degrees, final int[] colors)
			{
				if (colors != null && colors.length != values_degrees.length)
					throw new IllegalArgumentException("Parameter 'colors' must either be null or have the same size as parameter 'values_degrees'!");
				this.colors = colors;
				this.values_degrees = values_degrees;
				this.invalidate();
			}

		public static float[] calculateDegrees(float[] values)
			{
				float[] data=new float[values.length];
				float total=0;
				for (int i=0;i < values.length;i++)
					{
						total += values[i];
					}
				for (int i=0;i < values.length;i++)
					{
						data[i] = 360 * (values[i] / total);
					}
				return data;
			}
	}
