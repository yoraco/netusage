package de.superfusion.android.netusage;
import android.os.*;
import android.util.*;
import android.view.*;
import android.widget.*;

public class AppinfoDetailsFragment extends android.support.v4.app.Fragment
implements AppinfoListFragment.AppinfoCommunicator
	{
		private static final String TAG="AppInfoDetails";
		TextView textViewName=null;
		TextView textViewTotal=null;
		ImageView imageViewIcon=null;
		TextView textViewUpload=null;
		TextView textViewDownload=null;
		GraphView graphView=null;
		View averageInline=null;
		View averageInlinePlaceholder=null;
		int colorUpload=0;
		int colorDownload=0;
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
			{
				View view=inflater.inflate(R.layout.appinfo_details_fragment, container, false);
				textViewName = (TextView)view.findViewById(R.id.appinfo_details_name);
				textViewTotal = (TextView)view.findViewById(R.id.appinfo_details_total);
				imageViewIcon = (ImageView)view.findViewById(R.id.appinfo_details_icon);
				textViewUpload = (TextView)view.findViewById(R.id.appinfo_details_upload);
				textViewDownload = (TextView)view.findViewById(R.id.appinfo_details_download);
				graphView = (GraphView)view.findViewById(R.id.appinfo_details_graphview);
				averageInline = view.findViewById(R.id.appinfo_details_average_percent_inline);
				averageInlinePlaceholder = view.findViewById(R.id.appinfo_details_average_percent_inline_ph);

				colorUpload = getResources().getColor(R.color.nu_graph_upload);
				colorDownload = getResources().getColor(R.color.nu_graph_download);

				return view;
			}

		@Override
		public void update(AppinfoListFragment.AppInfo appInfo)
			{
				if (null != appInfo)
					{
						textViewTotal.setText(appInfo.getTotalString());
						textViewName.setText(appInfo.label);
						imageViewIcon.setImageDrawable(appInfo.icon);
						textViewUpload.setText(appInfo.getUploadString());
						textViewDownload.setText(appInfo.getDownloadString());
						graphView.updateFromValues(new float[]{appInfo.uploaded,appInfo.downloaded}, new int[]{colorUpload,colorDownload});


						// use layout weight to set percentag 
						// by using parent views attribute 'weightSum' set to 100
						float left=appInfo.getPercentageFromTotal();
						if (left < 0.5)
							left = 0.5F;
						float right=100 - left;

						float layout_weight_left=right;
						float layout_weight_right=left;
						averageInline.setLayoutParams(
							new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
														  averageInline.getLayoutParams().height,
														  layout_weight_left
														  ));

						averageInlinePlaceholder.setLayoutParams(
							new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
														  averageInline.getLayoutParams().height,
														  layout_weight_right
														  ));						
					}
				else
					{
						textViewTotal.setText("No value");
						textViewName.setText("No title");
						imageViewIcon.setImageResource(android.R.drawable.sym_def_app_icon);	
						textViewUpload.setText("No value");
						textViewDownload.setText("No value");
						graphView.updateFromValues(new float[]{0,0}, null);
					}
			}
	}
