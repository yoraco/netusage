# README #

### What is this repository for? ###

* Simply shows all apps that cause internet traffic on Android device
* Example of using fragments for devices with different screen sizes
* Example of custom view like a pie chart.
* Example of using percentage for view width

### How do I get set up? ###

* Simply import this project as an eclipse project
* This project needs the android (compat) support v4 library

### Created with ###
* The whole project has been created on an Asus Transformer TF101 with AIDE.
